# Anti Money Laundering Ontology (AMLO)
__In order to let computers to work for us, they must understand data: not just the grammar and the syntax, but the real meaning of things.__ [[KEES](http://linkeddata.center/kees)]

AMLO is a family of formal conceptual models to support Anti Money Laundering (AML) investigations. 

AMLO is developed by a multidisciplinary group of domain experts and data scientists as part of the Mopso project hosted by [LinkedData.Center](https://LinkedData.Center).

As today, the following modules are published:

- **[AMLO core](core/)**  that extends
the *Financial Industry Business Ontology (FIBO)* with new concepts needed to describe the AML knowledge and facts.
- **[AMLO social network](sn/)**  that defines a simplified abstract social network graph for AML domain.
- **[AMLO verifiable credentials](vc/)**: a proposal for an ontology for verifiable credentials in AML.



## Conformance
This repository conforms to [library/owl](https://doc.mopso.io/reference/library/owl) requirements and to the [Principles of best practices for FIBO](https://github.com/edmcouncil/fibo/blob/master/ONTOLOGY_GUIDE.md).



## What is an ontology?
An ontology in OWL is made up of statements about Classes (i.e., sets of things) and Properties 
(ways that things relate to other things). 
AMLO defines the sets of things that are of interest in Anti Money Laundering (AML), 
and the ways that those things can relate to one another. 
In this way, AMLO can give meaning to any data (e.g., spreadsheets, relational databases, XML documents) that describe the AML activities. 

AMLO considers both Classes and Properties to be Concepts. The languages of Ontologies were originally developed by the US DoD and
are codified by the World Wide Web Consortium (W3C).



## Quickstart for developers

```bash
docker run --rm -ti -p 8080:8080 -v ${PWD}:/workspace linkeddatacenter/sdaas-ce:3.3.1
SD_LEARN http://w3id.org/amlo/core core/core.rdf
SD_LEARN http://w3id.org/amlo/sn sn/sn.rdf
# browse http://localhost:8080/sdaas
SD_INCLUDE testing
SD_DATA_TEST
exit
```

## References
This work was inspired by:

- Carnaz, Gonçalo & Nogueira, Vitor & Antunes, Mario. (2017). *Ontology-Based Framework Applied to Money Laundering Investigations.* 
- Murad Mehmet and Duminda Wijesekera . *Ontological Constructs to Create Money Laundering*
- Roberto Basili, Marco Cammisa, Maria Vittoria Marabello, Marco Pennacchiotti, Dario Saracino, and Fabio Massimo Zanzotto.
*Ontology-driven Information Retrieval in FF-Poirot*
- Quratulain Rajput1, Nida Sadaf Khan1, Asma Larik1 & Sajjad Haider1. *Ontology Based Expert-System for Suspicious Transactions Detection*
- Perera K., Karunarathne D.D., Siriwardena A. , Balaretnaraja D. *Ontology Based Annotation Mechanism for Financial Documents*
- E.Fagnoni. *Financial Report Vocabulary*, https://github.com/linkeddatacenter/BOTK-fr
- Allemang D., Hendler J. *Semantic web for the working ontologist*, 2011
- GraphML Team  *The GraphML File Format* 2019, http://graphml.graphdrawing.org/ 


## License
© Copyright by [MOPSO](https://mopso.eu/). All right reserved. See LICENSE file.
